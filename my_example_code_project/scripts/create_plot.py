import typing as t

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sklearn.linear_model


def create_noisy_linear_data(
    n_points: int, slope: float, stddev: float
) -> t.Tuple[np.ndarray, np.ndarray]:
    """
    Create data points (x,y) from linear function with added noise.

    Parameters
    ----------
    n_points: Number of data points to generate.
    slope: Slope of the underlying linear function.
    stddev: Standard deviation of added white noise (mean is 0).

    Returns
    -------
    Data pairs (x,y).
    """
    x = np.arange(n_points)
    y = slope * x + np.random.normal(scale=stddev, size=n_points)
    return x, y


def plot_data(
    ax: matplotlib.axes.Axes, x: np.array, y: np.array
) -> matplotlib.axes.Axes:
    """Plot data points."""
    ax.plot(x, y, ".")
    return ax


def perform_linear_regression(
    x: np.array, y: np.array
) -> sklearn.linear_model.LinearRegression:
    """Perform linear regression."""
    regressor = sklearn.linear_model.LinearRegression()
    regressor.fit(x.reshape(-1, 1), y)
    return regressor


def plot_regression(
    ax: matplotlib.axes.Axes,
    regression: sklearn.linear_model.LinearRegression,
    x: np.array,
    y: np.array,
) -> matplotlib.axes.Axes:
    """Plot a prediction made by the trained regressor."""
    ax.plot(regression.predict(x.reshape(-1, 1)))
    return ax


if __name__ == "__main__":
    x, y = create_noisy_linear_data(100, 1, 2)
    regression = perform_linear_regression(x, y)

    fig, ax = plt.subplots()
    plot_data(ax, x, y)
    plot_regression(ax, regression, x, y)
    fig.savefig("plot.png")
