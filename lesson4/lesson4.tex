\section{Gitlab runners}

\subsection{What are runners for?}

Gitlab runners are responsible for the automated execution of CI/CD pipelines. gitlab runner itself is a go program, however the same term is often used to refer to the instances running the CI. gitlab runner supports multiple execution backends (executor) and can be installed locally or deployed to some remote compute resources-

\subsection{Local runner}

Local runners can be configured on your local machine. Running on those is available even in the free gitlab plan. Downside: Your local machine must be turned on and have internet access during pipeline execution. If your pipeline contains large workloads working on your local machine might become slow during pipeline execution.

\subsection{Remote runner}

Remote runners are deployed on remote machines, often times on servers or the Cloud. It is usually assumed that remote runners, if configured, are available all the time. It is not uncommon to have dedicated machines / provisioned resources that are solely reserved for runners. The upside is that you don't block yourself with those, the downside is that you need to provision remote resources somehow.

Note: In gitlab.com there is a free plan for a few free monthly runner minutes. You must be registered and authenticated though.

\subsection{Installing and registering a runner}

The - arguably - easiest and most flexible way to install gitlab runner is to use the official docker image. Installation then boils down to running the container with appropriate flags:

\begin{lstlisting}[language=bash]
$ (sudo) docker run \
    -d --name gitlab-runner \
    --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
\end{lstlisting}

The flags have the following effects:

\begin{itemize}
	\item \verb|-d|: Detach process from terminal
	\item \verb|--name|: Name the container
	\item \verb|--restart always|: Always restart if stopped
	\item \verb|-v /srv/gitlab-runner/config:/etc/gitlab-runner|: Mount configuration into docker container
	\item \verb|-v /var/run/docker.sock:/var/run/docker.sock|: Give container access to system-wide docker daemon
\end{itemize}

To register the runner, execute the following:

\begin{lstlisting}[language=bash]
$ (sudo) docker exec -it gitlab-runner gitlab-runner register
\end{lstlisting}

and follow general instructions on registering.

\subsection{Executors}

Executors determine against which backend runners execute the CI.

\subsubsection{Docker executor}

The docker executor uses docker containers for pipeline execution. For each job a dedicated container is started. Container images can be chosen in the job's image section.

The docker executor is most commonly used and most suitable for local runners because of its flexibility and encapsulated environments in containers.

\subsubsection{Shell executor}

The shell executor can be used to run CI jobs directly from the host system. 

This executor is useful when programs that are needed are not available in Docker images (e.g. with proprietary software) or if deployment of complex architectures is tested or needed for integration testing.

Note that with the shell executor you might alter your host system, e.g. when installing software for CI jobs.


\subsection{Runners and Merge Requests}

Running the CI/CD pipeline plays a vital role in the professional usage of gitlab, most importantly when it comes to handling merge requests.

It is a best practice to disable merging when the pipeline has not succeeded. Therefore, availability of gitlab runners is crucial to the overall gitlab workflow.

In the most simple case all available runners can handle all defined jobs. However, there are scenarios in which this is not true. Jobs can be configured with rules that define conditions under which the jobs are executed or skipped. Additionally, the runner to be used can be specified. Two use-cases are highlighted below.

\subsubsection{Use case I: Integration test}

The term integration test refers to testing a software system as a whole. Modern systems often require databases and other services to be running or are constructed entirely in a microservice architecture, i.e. as a number of small, individual components that communicate via APIs.

For a test of the full functionality, the whole system needs to be available. Typically this cannot be achieved trivially within a docker container and thus the docker executor might not be the ideal choice for integration testing.

Furthermore, integration tests often take long to spin up and execute. They can often be skipped when a simple commit has been pushed, but are vital for quality control before a merge request can be closed.

In this case, a strategy would be to:

\begin{itemize}
	\item Provide proper rules for integration test jobs.
	\item Define a dedicated runner for these jobs that will not be blocked by lengthy jobs.
	\item Use a shell executor on the dedicated runner to spin up all required services.
\end{itemize}

\subsubsection{Use case II: Data accessibility}

Some CI jobs might need input data to perform correctly - e.g. when generating diagrams for a LaTeX document that is built in the pipeline.

It is bad practice to check large files into git.\footnote{git LFS (large file storage) provides a way to track larger files such as images. It is however still impractical and not recommended to track datasets etc. this way.}

Typically, data are not available for every runner. However, specifying a runner on the machine where data are stored and giving it access to these data ensures that diagrams can be built.
