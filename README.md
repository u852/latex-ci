# latex-ci

[Link to lecture notes](https://u852.gitlab.io/latex-ci/main.pdf)
[Link to example document with diagram](https://u852.gitlab.io/latex-ci/example.pdf)

## Build tex

```commandline
sudo docker run -v $(pwd):/data rekka/tectonic /data/lesson1.tex
```
